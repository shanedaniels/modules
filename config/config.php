<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Paths
    |--------------------------------------------------------------------------
    |
    | Base paths to directories containing extensions
    |
    */
    'paths' => [
        base_path('modules'),
        base_path('workbench')
    ],

    /*
    |--------------------------------------------------------------------------
    | Manifest
    |--------------------------------------------------------------------------
    |
    | Name of and depth at which to look for the module's manifest file.
    | The manifest contains information needed to load the module.
    |
    */
    'manifest' => [
        'filename' => 'module.json',
        'depth' => 2,
        'schema' => base_path('workbench/shanedaniels/modules/resources/manifest_schema.json')
    ],




];