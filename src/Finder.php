<?php namespace ShaneDaniels\Modules;

use Illuminate\Filesystem\Filesystem;
use ShaneDaniels\Modules\Contracts\Finder as FinderContract;
use Symfony\Component\Finder\Finder as SymfonyFinder;

class Finder implements FinderContract {

    /**
     * Symfony Finder instance
     *
     * @var SymfonyFinder
     */
    protected $finder;

    /**
     * Array of paths containing modules.
     *
     * @var array
     */
    protected $paths = [];

    /**
     * Name of the file containing a module's metadata
     *
     * @var string
     */
    protected $manifestFilename = 'module.json';

    /**
     * Depth in folder structure to look for a module's
     * manifest file.
     *
     * @var int
     */
    protected $manifestDepth = 2;

    /**
     * @param SymfonyFinder $finder
     */
    public function __construct(SymfonyFinder $finder, array $paths = [])
    {
        $this->finder = $finder;

        $this->paths = $paths;
    }

    /**
     * {@inheritDoc}
     */
    public function find()
    {
        $modules = [];

        foreach ($this->paths as $path)
        {
            $modules = array_merge($modules, $this->findInPath($path));
        }

        return $modules;
    }

    /**
     * {@inheritDoc}
     */
    public function findInPath($path)
    {
        $paths = [];

        $modules = $this->finder
            ->in($path)
            ->files()
            ->name($this->manifestFilename)
            ->depth($this->manifestDepth)
            ->followLinks();

        foreach($modules as $module)
        {
            $paths[] = ($module->getRealPath());
        }

       return $paths;
    }

    /**
     * Returns instance of the Symfony Finder
     *
     * @return SymfonyFinder
     */
    public function getFinder()
    {
        return $this->finder;
    }

    /**
     * Sets instance of the Symfony Finder
     * @param SymfonyFinder $finder
     */
    public function setFinder($finder)
    {
        $this->finder = $finder;
    }

    /**
     * {@inheritDoc}
     */
    public function getPaths()
    {
        return $this->paths;
    }

    /**
     * {@inheritDoc}
     */
    public function setPaths($paths)
    {
        $this->paths = $paths;
    }

    /**
     * {@inheritDoc}
     */
    public function addPath($path)
    {
        if ( ! in_array($path, $this->paths))
        {
            $this->paths[] = $path;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getManifestFilename()
    {
        return $this->manifestFilename;
    }

    /**
     * {@inheritDoc}
     */
    public function setManifestFilename($manifestFilename)
    {
        $this->manifestFilename = $manifestFilename;
    }

    /**
     * Returns the folder depth of our manifest file
     *
     * @return int
     */
    public function getManifestDepth()
    {
        return $this->manifestDepth;
    }

    /**
     * Sets the depth our our manifest file.
     *
     * @param int $manifestDepth
     */
    public function setManifestDepth($manifestDepth)
    {
        $this->manifestDepth = $manifestDepth;
    }

}