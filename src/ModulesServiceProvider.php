<?php namespace ShaneDaniels\Modules;

use ShaneDaniels\Support\ServiceProvider;
use Symfony\Component\Finder\Finder as SymfonyFinder;

class ModulesServiceProvider extends ServiceProvider {

    /**
     * Boot up our packages views, trans, and configs.
     */
    public function boot()
    {
        $this->package('shanedaniels/modules');

        $this->app['modules']->findAndRegister();
        $this->app['modules']->sortByDependency();

        foreach ($this->app['modules']->allEnabled() as $module)
        {
            $module->boot();
        }
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bindShared('modules.finder', function($app)
        {
            $paths = config('shanedaniels.modules.paths');

            $finder = new Finder(new SymfonyFinder, $paths);

            $finder->setManifestFilename(config('shanedaniels.modules.manifest.filename'));
            $finder->setManifestDepth(config('shanedaniels.modules.manifest.depth'));

            return $finder;
        });

        $this->app->bindShared('modules', function($app)
        {
            $modules = new Modules;

            $modules->setContainer($app);
            $modules->setFinder($app['modules.finder']);
            $modules->setManifestSchema(config('shanedaniels.modules.manifest.schema'));

            return $modules;
        });
    }
}