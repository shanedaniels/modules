<?php namespace ShaneDaniels\Modules;

use Illuminate\Container\Container;
use Illuminate\Support\Collection;
use JsonSchema\Validator;
use RuntimeException;
use ShaneDaniels\Dependable\Sorter;

class Modules extends Collection {

    /**
     * Illuminate Container instance
     *
     * @var Container
     */
    protected $container;

    /**
     * @var Finder
     */
    protected $finder;

    /**
     * @var
     */
    protected $manifestSchema;

    /**
     * Returns all enabled extensions
     *
     * @return static
     */
    public function allEnabled()
    {
        $modules = $this->filter(function($module)
        {
            return $module->isEnabled();
        });

        return $modules;
    }

    /**
     * Returns all disabled extensions
     *
     * @return static
     */
    public function allDisabled()
    {
        $modules = $this->filter(function($module)
        {
            return $module->isDisabled();
        });

        return $modules;
    }

    /**
     * Returns all disabled extensions
     *
     * @return static
     */
    public function allBooted()
    {
        $modules = $this->filter(function($module)
        {
            return $module->isBooted();
        });

        return $modules;
    }

    /**
     * Returns all disabled extensions
     *
     * @return static
     */
    public function allNotBooted()
    {
        $modules = $this->filter(function($module)
        {
            return ! $module->isBooted();
        });

        return $modules;
    }

    /**
     *
     */
    public function findAndRegister()
    {
        $modules = $this->finder->find();

        foreach($this->finder->find() as $module)
        {
            $this->register($module);
        }
    }

    /**
     * Registers our model
     *
     * @param $module
     */
    public function register($module)
    {
        $module = $this->createInstance($module);

        $module->register();

        $this->items[$module->getSlug()] = $module;
    }

    /**
     * Creates a new instance of our Module
     *
     * @param $module
     * @return Module
     */
    protected function createInstance($module)
    {
        $attributes = $this->validateAndRetrieveManifestFile($module);

        $path = dirname($module);

        $attributes['author'] = (array) $attributes['author'];

        return new Module($this, $path, $attributes);
    }

    /**
     * Sorts our extensions based on dependencies
     *
     * @return void
     */
    public function sortByDependency()
    {
        if (!empty($this->items))
        {
            $this->items = (new Sorter($this->all()))->order()->all();
        }
    }

    /**
     * Build the package's namespace using the provided slug.
     *
     * We are assuming the standard {vendor}/{packageName}
     *
     * @param $slug
     * @return array
     */
    protected function getNamespaceFromSlug($slug)
    {
        list($vendor, $name) = array_map('studly_case', explode('/', $slug, 2));

        return "{$vendor}\\$name";
    }

    /**
     * Validates and Returns our Extension's manifest file.
     *
     * @param $file
     * @throws RuntimeException
     * @return array
     */
    protected function validateAndRetrieveManifestFile($file)
    {
        $validator = new Validator($file);

        $data = json_decode($this->container['files']->get($file));
        $schema = json_decode($this->container['files']->get($this->manifestSchema));

        $validator->check($data, $schema);

        if ( $validator->isValid())
        {
            return (array) $data;
        }

        $errorMsg = "Extension Manifest [$file] does not validate. \n\nViolations:\n";

        foreach ($validator->getErrors() as $error)
        {
            $errorMsg .= sprintf("%s\n", $error['message']);
        }

        throw new RuntimeException($errorMsg);
    }

    /**
     * Returns the schema file for extension manifest.
     *
     * @return mixed
     * @throws \Illuminate\Filesystem\FileNotFoundException
     */
    protected function getManifestSchemaFile()
    {
        return json_decode($this->filesystem->get($this->manifestSchema));
    }

    /**
     * Returns instance of the Illuminate Container
     *
     * @return Container
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * Sets instance of the Illuminate Container
     *
     * @param Container $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * Returns instance of the Module Finder
     *
     * @return Finder
     */
    public function getFinder()
    {
        return $this->finder;
    }

    /**
     * Sets instance of the Module Finder
     *
     * @param Finder $finder
     */
    public function setFinder($finder)
    {
        $this->finder = $finder;
    }

    /**
     * @return mixed
     */
    public function getManifestSchema()
    {
        return $this->manifestSchema;
    }

    /**
     * @param mixed $manifestSchema
     */
    public function setManifestSchema($manifestSchema)
    {
        $this->manifestSchema = $manifestSchema;
    }
}