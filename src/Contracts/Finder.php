<?php namespace ShaneDaniels\Modules\Contracts;

interface Finder {

    /**
     * Returns an array of modules located in the given
     * array of paths.
     *
     * @return array
     */
    public function find();

    /**
     * Returns an array of modules that are located
     * in the given path.
     *
     * @param $path
     * @return array
     */
    public function findInPath($path);

    /**
     * Returns array of paths containing modules
     *
     * @return array
     */
    public function getPaths();

    /**
     * Sets array of paths containing modules
     *
     * @param array $paths
     */
    public function setPaths($paths);

    /**
     * Adds a new path to the array of paths, if
     * it does not already exist.
     *
     * @param $path
     */
    public function addPath($path);

    /**
     * Returns the name of the file that contains a
     * module's metadata.
     *
     * @return string
     */
    public function getManifestFilename();

    /**
     * Sets the name of the file containing a module's
     * metadata.
     *
     * @param string $metadataFilename
     */
    public function setManifestFilename($metadataFilename);
}