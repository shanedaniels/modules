<?php namespace ShaneDaniels\Modules;

use Illuminate\Container\Container;
use RuntimeException;
use ShaneDaniels\Dependable\Dependable;

/**
 * Class Module
 * @package Mango\Modules
 */
class Module implements Dependable {

    /**
     * Modules Instance
     *
     * @var Modules
     */
    protected $modules;

    /**
     * Base path to the module's folder
     *
     * @var string
     */
    protected $path;

    /**
     * Array containing the module's attributes.
     *
     * @var array
     */
    protected $attributes = [];

    /**
     * @var string
     */
    protected $manifest;

    /**
     * Attributes that will not be published when
     * converted to an array or json.
     *
     * @var array
     */
    protected $hiddenAttributes = ['path'];

    /**
     * Flag for whether extension has been registered
     * with the collection
     *
     * @var bool
     */
    protected $registered = false;

    /**
     * Flag for whether the extension has been booted.
     *
     * @var bool
     */
    protected $booted = false;

    /**
     * @param $path
     * @param $attributes
     */
    public function __construct(Modules $modules, $path, $attributes)
    {
        $this->modules = $modules;

        $this->path = $path;

        $this->attributes = $attributes;
    }

    /**
     * Returns the extension's slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Returns the extension's dependencies
     *
     * @return string
     */
    public function getDependencies()
    {
        return $this->dependencies ?: [];
    }

    /**
     * Returns true is the extension is enabled
     *
     * @return bool
     */
    public function isEnabled()
    {
        return (boolean) $this->enabled;
    }

    /**
     * Returns true is the extension is not enabled
     *
     * @return bool
     */
    public function isDisabled()
    {
        return (boolean) ! $this->enabled;
    }

    /**
     * Returns true is the extension is booted
     *
     * @return bool
     */
    public function isBooted()
    {
        return (boolean) $this->booted;
    }

    /**
     * Returns true if we have providers in our manifest and
     * false if we do not :)
     * @return bool
     */
    public function hasProviders()
    {
        return ! empty($this->providers);
    }

    /**
     * Registers a new function
     *
     * Available Events:
     * - RegisteringExtension
     * - FinishedRegisteringExtension
     *
     * @return void
     */
    public function register()
    {
        $container = $this->modules->getContainer();

        if ($this->hasProviders())
        {
            foreach ($this->providers as $provider)
            {
                $container->resolveProviderClass($provider)->register();
            }
        }

        $this->registered = true;
    }

    /**
     * Boots up our extension
     *
     * Available Events:
     * - BootingExtension
     * - FinishedBootingExtension
     */
    public function boot()
    {
        if  ( ! $this->enabled)
        {
            throw new RuntimeException("Extension [{$this->slug}] must be enabled before it can be booted.");
        }

        $container = $this->modules->getContainer();

        if ($this->hasProviders())
        {
            foreach ($this->providers as $provider)
            {
                $container->resolveProviderClass($provider)->boot();
            }
        }

        $this->loadRoutes();

        $this->booted = true;
    }

    /**
     * Writes updates to the extension's manifest file
     *
     * @param $attributes
     */
    public function updateManifest($attributes)
    {
        $this->attributes = array_merge($this->attributes, $attributes);

        $files = $this->getContainer()['files'];

        $files->put(
            $this->path . '/' . $this->getFinder()->getManifestFilename(),
            $this->toJson(JSON_PRETTY_PRINT)
        );
    }

    /**
     * Loads our routes file
     *
     * @throws \Illuminate\Filesystem\FileNotFoundException
     */
    public function loadRoutes()
    {

        $path = $this->path . '/src/Http/routes.php';

        if (is_file($path))
        {
            require $path;
        }
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * Returns the path to the module's manifest file.
     *
     * @return string
     */
    public function getManifest()
    {
        return $this->manifest;
    }

    /**
     * Sets the path to the module's manifest file.
     *
     * @param string $manifest
     */
    public function setManifest($manifest)
    {
        $this->manifest = $manifest;
    }

    /**
     * Returns array of hidden attributes
     *
     * @return array
     */
    public function getHiddenAttributes()
    {
        return $this->hiddenAttributes;
    }

    /**
     * Sets the array of hidden attributes
     *
     * @param array $hiddenAttributes
     */
    public function setHiddenAttributes($hiddenAttributes)
    {
        $this->hiddenAttributes = $hiddenAttributes;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param array $attributes
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * Sets a given attribute for the extension.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return void
     */
    public function setAttribute($key, $value)
    {
        $this->attributes[$key] = $value;
    }

    /**
     * Returns the given attribute.
     *
     * @param  string  $key
     * @param  mixed  $default
     * @return mixed
     */
    public function getAttribute($key, $default = null)
    {
        if (array_key_exists($key, $this->attributes))
        {
            return $this->attributes[$key];
        }
        return value($default);
    }

    /**
     * Dynamically retrieve attributes on the object.
     *
     * @param  string  $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->getAttribute($key);
    }

    /**
     * Dynamically set attributes on the object.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return void
     */
    public function __set($key, $value)
    {
        $this->setAttribute($key, $value);
    }

    /**
     * Determines if an attribute exists on the object.
     *
     * @param  string  $key
     * @return void
     */
    public function __isset($key)
    {
        return isset($this->attributes[$key]);
    }

    /**
     * Unset an attribute on the object.
     *
     * @param  string  $key
     * @return void
     */
    public function __unset($key)
    {
        unset($this->attributes[$key]);
    }
    /**
     * Returns the extension's attributes as an array
     *
     * @return array
     */
    public function toArray()
    {
        $attributes = array_except($this->attributes, $this->hiddenAttributes);

        foreach ($attributes as $key => $value)
        {
            $properties[$key] = $value;
        }

        return $properties;
    }

    /**
     * Returns our extension's attributes as json
     *
     * @param null $options
     * @return string
     */
    public function toJson($options = null)
    {
        return json_encode($this->toArray(), $options);
    }
}